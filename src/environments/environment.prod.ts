export const environment = {
  apiUrl: 'https://stop-motion.web-punks.com/api',
  production: true,
  version: '1.1.0'
};
