export interface Enviroment {
    apiUrl: string;
    production: boolean;
    version: string;
}
